import React from "react";
import { render, fireEvent, screen, getByTestId } from "@testing-library/react";
import Dashboard from "../Containers/Dashboard";
import useCalculateCart from "../hooks/useCalculateCart";
import { UserTypes } from "../Containers/User/user.interface";
import { CartItem } from "../Containers/Cart/cart.interface";
import { MenuItem, PizzaType } from "../Containers/Pizza/pizza.interface";
import { pizzaTypes } from "../Containers/Pizza/pizza.constant";



describe('Render Dashboard', () => {
  it(`should render dashboard component`, () => {
    render(<Dashboard />);
  });
});


describe('Pizza Values Test', () => {
  let selectedUserType: UserTypes;
  let cart: CartItem[] = []
  let cartValue: number = 0
  const setCartValue = (cartVal: number) => cartValue = cartVal
  const mockSmallPizza = pizzaTypes.find((pizza: MenuItem) => pizza.name === PizzaType.SmallPizza)
  const mockMediumPizza = pizzaTypes.find((pizza: MenuItem) => pizza.name === PizzaType.MediumPizza)
  const mockLargePizza = pizzaTypes.find((pizza: MenuItem) => pizza.name === PizzaType.LargePizza)


  beforeEach(() => {
    cart = []
    cartValue = 0
  });
  it(`default user`, async () => {
    selectedUserType = UserTypes.Default;
    const { calculateCartValue } = useCalculateCart(cart, selectedUserType, cartValue, setCartValue);
    if (mockSmallPizza && mockMediumPizza && mockLargePizza) {
      await cart.push({ ...mockSmallPizza, quantity: 1 });
      await cart.push({ ...mockMediumPizza, quantity: 1 });
      await cart.push({ ...mockLargePizza, quantity: 1 });
    }
    await calculateCartValue()
    expect(cartValue).toEqual(49.97)
  })

  it(`Microsoft user`, async () => {
    selectedUserType = UserTypes.Microsoft;
    const { calculateCartValue } = useCalculateCart(cart, selectedUserType, cartValue, setCartValue);
    if (mockSmallPizza && mockMediumPizza && mockLargePizza) {
      await cart.push({ ...mockSmallPizza, quantity: 3 });
      await cart.push({ ...mockLargePizza, quantity: 1 });
    }
    await calculateCartValue()
    expect(cartValue).toEqual(45.97)
  })

  it(`Amazon user`, async () => {
    selectedUserType = UserTypes.Amazon;
    const { calculateCartValue } = useCalculateCart(cart, selectedUserType, cartValue, setCartValue);
    if (mockSmallPizza && mockMediumPizza && mockLargePizza) {
      await cart.push({ ...mockMediumPizza, quantity: 3 });
      await cart.push({ ...mockLargePizza, quantity: 1 });
    }
    await calculateCartValue()
    expect(cartValue).toEqual(67.96)
  })
});
