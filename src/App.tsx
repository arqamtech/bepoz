import React from "react";
import "./App.css";
import { Container } from "@material-ui/core";
import Dashboard from "./Containers/Dashboard";
import Header from "./Components/Header";

function App() {
  return (
    <Container>
      <Header />
      <Dashboard />
    </Container>
  );
}

export default App;
