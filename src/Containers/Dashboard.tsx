import { useState, useEffect } from "react";
import { Container, Typography, Box } from "@material-ui/core";
import SelectUser from "./User/SelectUser";
import SelectPizza from "./Pizza/SelectPizza";
import CartView from "./Cart/CartView";
import useCartHook from "../hooks/useCartHook";
import useCalculateCart from "../hooks/useCalculateCart";
import { UserTypes } from "./User/user.interface";
import { CartItem } from "./Cart/cart.interface";



function Dashboard() {
  const [selectedUserType, setSelectedUserType] = useState<UserTypes>(UserTypes.Default);
  const [cart, setCart] = useState<CartItem[]>([]);
  const [cartValue, setCartValue] = useState(0);
  const [totalQuantity, setTotalQuantity] = useState(0);

  const updateCart = async (cartItems: CartItem[]) => {
    console.log('cartItems', cartItems)
    await setCart(cartItems)
    calculateTotalPizzas()
  }

  const calculateTotalPizzas = async () => {
    let tempTotal: number = 0
    await cart.map(cartItem => {
      tempTotal += cartItem.quantity
    })
    await setTotalQuantity(tempTotal)
  }
  const { addPizza, removePizza } = useCartHook(cart, updateCart);
  const { calculateCartValue } = useCalculateCart(cart, selectedUserType, cartValue, setCartValue);

  useEffect(() => {
    calculateCartValue();
  }, [...cart.map(cartitem => cartitem.quantity), selectedUserType]);
  return (
    <Container style={{ marginTop: 100 }}>
      <Box component="span" m={1}>
        <SelectUser
          selectedUserType={selectedUserType}
          setSelectedUserType={setSelectedUserType}
        />
      </Box>
      <Box component="span" m={1}>
        <SelectPizza addPizza={addPizza} />
      </Box>
      <Box component="span" m={1} key={totalQuantity} >
        <CartView
          cartItems={cart}
          key={totalQuantity}
          addPizza={addPizza}
          totalPizzas={totalQuantity}
          removePizza={removePizza}
        />
      </Box>
      <Box component="span" mt={3}>
        <Typography variant="h3" >
          Price  : ${cartValue}
        </Typography>
      </Box>
    </Container>
  );
}

export default Dashboard;
