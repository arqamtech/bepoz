import { UserTypes } from "./user.interface";

export const userTypes = [
  { value: UserTypes.Default, label: UserTypes.Default },
  { value: UserTypes.Amazon, label: UserTypes.Amazon },
  { value: UserTypes.Facebook, label: UserTypes.Facebook },
  { value: UserTypes.Microsoft, label: UserTypes.Microsoft },
];
