import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import { userTypes } from "./user.constant";
import { SelectUserProps } from "./user.interface";
import Select from "react-select";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    width: '80%'
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

export default function SelectUser({
  selectedUserType,
  setSelectedUserType
}: SelectUserProps) {

  const classes = useStyles();
  const handleChange = (event: any) => {
    setSelectedUserType(event.value);
  };

  return (
    <div>
      <FormControl className={classes.formControl}>
        <InputLabel>Select User Type</InputLabel>
        <Select
          onChange={handleChange}
          value={userTypes?.filter((userType) => userType.value === selectedUserType)}
          options={userTypes}
          style={{
            width: '80%'
          }}
        />
      </FormControl>
    </div>
  );
}
