export enum UserTypes {
  Default = "Default",
  Microsoft = "Microsoft",
  Amazon = "Amazon",
  Facebook = "Facebook",
}
export interface SelectUserProps {
  selectedUserType: UserTypes | null;
  setSelectedUserType: (userType: any) => void;
}
