import { MenuItem } from "../Pizza/pizza.interface";

export interface CartViewProps {
  cartItems: CartItem[];
  removePizza: (pizzaType: MenuItem) => void;
  addPizza: (pizzaType: MenuItem) => void;
  totalPizzas?: number;
}
export interface CartItem extends MenuItem {
  quantity: number;
}
