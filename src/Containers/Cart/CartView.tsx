import { makeStyles } from "@material-ui/core/styles";
import { } from "../Pizza/pizza.interface";
import { CartViewProps, CartItem } from "./cart.interface";
import { Container, Card, CardActionArea, Typography, CardContent, IconButton, ButtonGroup } from '@material-ui/core'
import AddCircleRoundedIcon from '@material-ui/icons/AddCircleRounded';
import RemoveCircleRoundedIcon from '@material-ui/icons/RemoveCircleRounded';

const useStyles = makeStyles((theme) => ({
  cartView: {
    display: "flex",
    flexDirection: "column",
  },
  trashButton: {
    float: 'right'
  },
  cardContent: {
    float: 'left'
  }
}));

export default function SelectPizza({
  cartItems,
  removePizza,
  addPizza,
  totalPizzas
}: CartViewProps) {

  const classes = useStyles();

  return (
    <Container className={classes.cartView} >
      {cartItems.map((pizza: CartItem, idx) => {
        return (
          <Card key={pizza.name + idx} >
            <CardActionArea>
              <CardContent className={classes.cardContent} >
                <Typography gutterBottom variant="h5" component="h2">
                  {pizza.name} (${pizza.price})
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  {pizza.description}
                </Typography>

              </CardContent>
              <CardContent className={classes.trashButton} >
                <ButtonGroup color="primary" aria-label="outlined primary button group">
                  <IconButton onClick={() => removePizza(pizza)} color="secondary" >
                    <RemoveCircleRoundedIcon fontSize="large" />
                  </IconButton>
                  <Typography variant="body2" color="textSecondary" component="p">
                    {pizza.quantity}
                  </Typography>
                  <IconButton onClick={() => addPizza(pizza)} color="primary" >
                    <AddCircleRoundedIcon fontSize="large" />
                  </IconButton>
                </ButtonGroup>
              </CardContent>
            </CardActionArea>
          </Card>
        )
      })}
    </Container>


  );
}
