import { PizzaType, MenuItem } from "./pizza.interface";

export const pizzaTypes: MenuItem[] = [
  {
    id: "small",
    name: PizzaType.SmallPizza,
    description: "10'' pizza for one person",
    price: 11.99,
  },
  {
    id: "medium",
    name: PizzaType.MediumPizza,
    description: "12'' Pizza for two persons",
    price: 15.99,
  },
  {
    id: "large",
    name: PizzaType.LargePizza,
    description: "15'' Pizza for four persons",
    price: 21.99,
  },
];
