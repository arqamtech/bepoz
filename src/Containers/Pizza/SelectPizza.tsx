import { makeStyles } from "@material-ui/core/styles";
import { pizzaTypes } from "./pizza.constant";
import { MenuItem, SelectPizzaProps } from "./pizza.interface";
import { Container, Card, CardActionArea, Typography, CardContent, CardActions, Button } from '@material-ui/core'


import AddIcon from '@material-ui/icons/Add';
const useStyles = makeStyles((theme) => ({
  cardContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-evenly"
  },
  card: {
    width: '25%'
  },
  trashButton: {
    float: 'right'
  },
  cardContent: {
    float: 'left'
  }
}));

export default function SelectPizza({
  addPizza
}: SelectPizzaProps) {

  const classes = useStyles();

  return (
    <Container className={classes.cardContainer} >
      {pizzaTypes.map((pizza: MenuItem) => {
        return (

          <Card key={pizza.name} className={classes.card}>
            <CardActionArea>
              <CardContent className={classes.cardContent} >
                <Typography gutterBottom variant="h5" component="h2">
                  {pizza.name}
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  {pizza.description}
                </Typography>
              </CardContent>
              <CardContent className={classes.trashButton} >
                <Typography gutterBottom variant="h5" component="h2">
                  ${pizza.price}
                </Typography>
              </CardContent>
            </CardActionArea>
            <CardActions>
              <Button
                startIcon={<AddIcon />}
                fullWidth
                variant="contained"
                onClick={() => addPizza(pizza)}
                color="primary">
                Add to Cart
              </Button>
            </CardActions>
          </Card>
        )
      })}
    </Container>


  );
}
