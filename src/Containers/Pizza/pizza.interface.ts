export enum PizzaType {
  SmallPizza = "Small Pizza",
  MediumPizza = "Medium Pizza",
  LargePizza = "Large Pizza",
}
export interface SelectPizzaProps {
  addPizza: (pizzaType: MenuItem) => void;
}
export interface MenuItem {
  id: string;
  name: string;
  description: string;
  price: number;
}

