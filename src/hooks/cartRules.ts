export const cartRules = {
  Default: {
    small: null,
    medium: null,
    large: null,
  },
  Microsoft: {
    small: {
      requiredQuantity: 3,
      pricedForQuantity: 2,
      specialPrice: null,
    },
    medium: null,
    large: null,
  },
  Amazon: {
    small: null,
    medium: null,
    large: {
      requiredQuantity: null,
      pricedForQuantity: null,
      specialPrice: "19.99",
    },
  },
  Facebook: {
    small: null,
    medium: {
      requiredQuantity: 5,
      pricedForQuantity: 4,
      specialPrice: null,
    },
    large: null,
  },
};
