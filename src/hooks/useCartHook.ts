import { useReducer } from "react";
import { MenuItem } from "../Containers/Pizza/pizza.interface";
import { CartItem } from "../Containers/Cart/cart.interface";

export default function useCartHook(
  cart: CartItem[],
  setCart: (cartItems: CartItem[]) => void
) {
  const addPizza = async (pizza: MenuItem) => {
    const isPizzaExists = cart.findIndex(
      (cartItem: CartItem) => cartItem.id === pizza.id
    );
    if (isPizzaExists > -1) {
      let tempCart = cart;
      tempCart[isPizzaExists].quantity++;
      await setCart(tempCart);
    } else {
      const cartitem: CartItem = { ...pizza, quantity: 1 };
      await setCart([...cart, cartitem]);
    }
  };

  const removePizza = (pizza: MenuItem) => {
    const isPizzaExists = cart.findIndex(
      (cartItem) => cartItem.id === pizza.id
    );
    if (cart[isPizzaExists].quantity > 1) {
      let tempCart = cart;
      tempCart[isPizzaExists].quantity--;
      setCart(tempCart);
    } else {
      let tempCart = cart;
      tempCart.splice(isPizzaExists, 1);
      setCart(tempCart);
    }
  };

  return { addPizza, removePizza };
}
