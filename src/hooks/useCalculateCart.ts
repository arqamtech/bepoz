import { useState } from "react";
import { CartItem } from "../Containers/Cart/cart.interface";
import { UserTypes } from "../Containers/User/user.interface";
import { cartRules } from "./cartRules";
export default function useCalculateCart(
  cart: CartItem[],
  selectedUserType: UserTypes | null,
  cartValue: number,
  setCartValue: Function
) {
  const calculateCartValue = async () => {
    let sum = 0;
    if (selectedUserType) {
      const userRule: any = cartRules[selectedUserType];
      await cart.forEach(({ price, quantity, id }: CartItem) => {
        let pizzaRuleForUser = userRule[id];
        if (pizzaRuleForUser && pizzaRuleForUser.specialPrice) {
          sum += pizzaRuleForUser.specialPrice * quantity;
        } else {
          if (
            pizzaRuleForUser &&
            quantity === pizzaRuleForUser.requiredQuantity
          ) {
            sum += price * pizzaRuleForUser.pricedForQuantity;
          } else {
            sum += price * quantity;
          }
        }
      });
      setCartValue(Math.round((sum + Number.EPSILON) * 100) / 100);
    }
  };

  return { cartValue, calculateCartValue };
}
