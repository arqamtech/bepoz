
# Bepoz

  

Pizza Order App
Special offers and prices for Corporate Employees

## Tests and Values

Privileged customers:
1. **Microsoft**: Gets a 3 for 2 deal for Small Pizzas
2. **Amazon**: Gets a discount on Large Pizza where the price drops to $19.99 per pizza
3. **Facebook**:  Gets a 5 for 4 deal on Medium Pizza


### Case #1
Customer: *default*
Items:
1. Small Pizza x1
2. Medium Pizza x1
3. Large Pizza x1
Output: Total **$49.97**

### Case #2
Customer: *Microsoft*
Items:
1. Small Pizza x3
2. Large Pizza x1
Output: Total **$45.97**

### Case #3
Customer: *Amazon*
Items:
1. Medium Pizza x3
2. Large Pizza x1
Output: Total **67.96**

## Available Scripts

  

In the project directory, you can run:

  

### `yarn start`

  

Runs the app in the development mode.\

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

  

The page will reload if you make edits.\

You will also see any lint errors in the console.

  

### `yarn test`

  

Launches the test runner in the interactive watch mode.\

See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

  

### `yarn build`

  

Builds the app for production to the `build` folder.\

It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\

Your app is ready to be deployed!